var notes = {};
var searchNotes = [];
var filteredContent = [];

exports.list = function(req, res) {
  res.json(Object.keys(notes));
}
exports.get = function(note_name, req, res) {
  if (!notes.hasOwnProperty(note_name)) {
    res.status(404);
    res.end();
    console.error(note_name + " NOT FOUND!");
  } else {
    res.json(notes[note_name]);
  }
};

exports.insert = function(note_name, req, res) {
  if (notes.hasOwnProperty(note_name)) {
    res.status(409);
    return console.error(note_name + " ALREADY THERE!");
  } else {
    if (!req.body.content) {
      res.status(422);
      res.end();
      return console.error(note_name + " INCORRECT BODY JSON!");
    }
    notes[note_name] = { content: req.body.content, inserted: new Date() };
    res.end();
  }
};


exports.upsert = function(note_name, req, res) {
  if (notes.hasOwnProperty(note_name)) {
    notes[note_name] = { content: req.body.content, modified: new Date() };
    res.end();
  } else {
    this.insert(note_name, req, res);
  }
};

exports.delete = function(note_name, req, res){
  if (notes.hasOwnProperty(note_name)){
    delete notes[note_name];
    res.end();
  }
  else{
    res.status(404);
    res.end();
    return console.error(note_name + " NOTE NOT FOUND!");
  }
}

exports.search = function(search_name, req, res){

  filteredContent = [];
  searchNotes = Object.keys(notes);


  for(let i = 0; i < searchNotes.length; i++){
    var n = notes[searchNotes[i]].content.search(search_name);
    console.log(search_name);
    if(n >= 0){
      filteredContent.push({note_name: searchNotes[i] , content: notes[searchNotes[i]].content, inserted: notes[searchNotes[i]].inserted})
    }
    else if(n < 0){

    }
  }

  if(filteredContent != null){
    console.log(filteredContent);
    res.send(filteredContent);
  }
  else{
    res.status(404);
    console.error(search_name + " NOTE NOT FOUND!");
  }
  res.end();
}
